# Donar

## Quickstart

### Installation

The following steps are provided for [Fedora 29 Workstation](https://getfedora.org/fr/workstation/download/).
We assume you have two containers or two virtual machines or two physical machines.

To setup each machine, you should do:

```
sudo dnf install --refresh -y cmake gcc gcc-c++ ninja-build glib2-devel glib2 tor valgrind git net-tools nmap-ncat
git clone https://gitlab.inria.fr/qdufour/donar.git
cd donar
mkdir out && cd out && cmake -GNinja .. && ninja && sudo ninja install
```

### Commands

Now your machine is ready and you will be able to use the following commads:

 * `donar` is our main binary. It can be run as a client or a server.
 * `udpecho` is a simple udp server that send back the data sent to him.
 * `torecho` is a simple tcp server that send back the data sent to him + configure the tor daemon to generate a hidden service URL and be accessible on the Tor network.
 * `measlat` can be used in conjunction with `udpecho` or `torecho` to measure a Round Trip Time (RTT)

Try to run the previous commands in your terminal without any option, you will see their help.

At any moment, you can use the following commands that are not part of the project to understand what you are doing:

```bash
netstat -ulpn # Show programs that listen on an UDP port
netstat -tlpn # Show prograns that listen on a TCP port
nc 127.0.0.1 8000 # Connect via TCP on server 127.0.0.1 listening on port 8000
nc -u 127.0.0.1 8000 # Connect via UDP on server 127.0.0.1 listening on port 8000
```

### Introduction to the debug tools `udpecho` and `measlat`

Now let's start simple, we will launch our udp echo server and access it locally:

```bash
udpecho -p 8000 &
nc 127.0.0.1 8000
```

If you write some data on your terminal and press enter, you will see that your data has been repeated. Well done!

Now, instead of using `nc`, we will use `measlat` to use this echo server to measure latencies (make sure that `udpecho` is still running):

```bash
measlat -h 127.0.0.1 -p 8000 -t udp
```

`measlat` will send one packet to our udpecho server and wait to receive it back, measure the time it took, display it and exit.

You can use `measlat` more extensively by defining the number of measures to do, an interval and the size of the packet:

```bash
measlat -h 127.0.0.1 -p 8000 -t udp -c 10 -i 100 -s 150
```

### Introduction to `donar`

Now, let's introduce our main project.
First, kill all the remaining processes `killall udpecho measlat nc`.

*On both machine*  
Move to the donar repository root where you will see the `torrc_simple` file.
We will need to start by launching tor in a terminal: 
 
```bash
tor -f ./torrc_simple
```

*On machine A*  
Launch Donar server in a second terminal:

```bash
donar -s -a naive -e 3000 -r 3001
```

In a third terminal, launch your echo service:

```bash
udpecho -p 3000
```

Display the content of the file `onion_services.pub` that has been created in your working directory.

*On machine B*  
Copy the content of the file `onion_services.pub` that is on *machine A* to *machine B* in a file named `machine_a.pub`.
Now, run Donar client in a second terminal:

```bash
donar -c -a naive -o machine_a.pub -r 3000 -e 3001
```

In a third terminal, launch your echo service:

```bash
udpecho -p 3001
```

*On machine A*
You can access to the echo service from *machine B* by running:

```bash
nc 127.13.3.7 3001
# or
measlat -h 127.13.3.7 -p 3001 -t udp
```

*On machine B*
You can access to the echo service from *machine A* by running:

```bash
nc 127.13.3.7 3000
# or
measlat -h 127.13.3.7 -p 3000 -t udp
```

If it works, that's all! You are now mastering Donar!

## Linphone configuration

Choose a SIP UDP, Audio RTP/UDP and Video RTP/UDP that is different between your clients.
Go to manage account.
Add a new SIP proxy.

```
Username: <username>@127.13.3.7:5061
Proxy: 127.13.3.7:5060
Leave the rest empty.
Uncheck all the checkboxes.
```

You also need to say to Linphone that you are behind a NAT and put `127.13.3.7` as your public IP address.

## Docker build

```
sudo docker build -t registry.gitlab.inria.fr/qdufour/donar .
sudo docker push registry.gitlab.inria.fr/qdufour/donar
sudo docker pull registry.gitlab.inria.fr/qdufour/donar
```

```
mkdir -p ./{xp1-shared,xp1-res}
sudo chown -R 1000 ./{xp1-shared,xp1-res}

sudo docker run -t -i \
  --privileged \
  -v `pwd`/xp1-shared:/home/donar/shared \
  registry.gitlab.inria.fr/qdufour/donar \
  xp1-server
 
sudo docker run -t -i \
  --privileged \
  -v `pwd`/xp1-res:/home/donar/res \
  -v `pwd`/xp1-shared:/home/donar/shared \
  registry.gitlab.inria.fr/qdufour/donar \
  xp1-client 1000 100 100

```

## Run an XP instance

```
sudo ./scripts/xp1 1000 100 100
```

## Run instances in parallel

We generate the name of the algorithm to run on the right side of the parallel command.
The idea is to generate a sequence like the following: `orig naive rr rrh orig naive rr rrh orig...`.

```
parallel -j 12 bash -c './xp-instance-runner $1 6000 100 100' -- `xp0=orig xp1=naive xp2=rr xp3=rrh xp4=witness; for i in {0..99}; do q='xp'$((i % 5)); echo ${!q}; done`
parallel.moreutils -j 16 bash -c './xp-instance-runner $0 6000 100 100' -- `xp0=orig xp1=naive xp2=rr xp3=rrh xp4=witness; for i in {0..274}; do q='xp'$((i % 5)); echo ${!q}; done`
parallel.moreutils -j 16 bash -c './xp-instance-runner $0 6000 100 100' -- `for i in {0..55}; do echo -n 'orig naive rr rrh witness '; done`
```

Tests:

```
parallel.moreutils -j 16 bash -c './xp-instance-runner rr 6000 100 100' -- `seq 0 15`
```

## Helpers

Track measures that didn't finish:

```
ls | grep -P '^naive-|^rrh-|^rr-|^orig-' | while read n; do echo -n "$n "; tail -n1 $n/res/*.csv ; done | grep -v "Measurement done"
```

Check if timer's bug occured:

```
ls | grep -P '^naive-|^rrh-|^rr-|^orig-' | while read n; do echo "$n "; grep 'bug' $n/log/client-measlat-stderr.log; done
```

Check if a free() invalid pointer bug occured:

```
grep 'free' naive-*/log/*-donar-*.log
grep -rn 'free()' .
```

## Use a Linphone container

```
docker build -f linphone.dockerfile -t superboum/linphone .
```

Run it:

```
docker run \
  -ti \
  -e DISPLAY=$DISPLAY \
  -v /tmp/.X11-unix:/tmp/.X11-unix \
  --ipc=host \
  superboum/linphone \
  bash
```

http://gstreamer-devel.966125.n4.nabble.com/Need-help-with-using-OPUS-over-RTP-td4661409.html

```
# some sources
audiotestsrc
pulsesrc

# some sinks
pulsesink

# sender
gst-launch-1.0 \
  pulsesrc ! \
  audioconvert ! \
  opusenc audio-type=voice inband-fec=false frame-size=20 ! \
  rtpopuspay ! \
  udpsink host=127.0.0.1 port=5000

# receiver
gst-launch-1.0 \
  udpsrc port=5000 caps="application/x-rtp" ! \
  rtpjitterbuffer do-lost=true do-retransmission=false ! \
  rtpopusdepay ! \
  opusdec plc=true use-inband-fec=false ! \
  pulsesink

# both sides
export TARGET=192.168.1.1 
gst-launch-1.0 \
  autoaudiosrc ! \
    queue ! \
    audioresample ! \
    opusenc audio-type=voice inband-fec=FALSE frame-size=20 bitrate=64000 dtx=TRUE ! \
    rtpopuspay ! \
    udpsink host=$TARGET port=5000 async=FALSE \
  udpsrc port=5000 caps="application/x-rtp" ! \
    rtpjitterbuffer do-lost=TRUE do-retransmission=FALSE latency=10 ! \
    rtpopusdepay ! \
    opusdec plc=TRUE use-inband-fec=FALSE ! \
    audioresample ! \
    autoaudiosink

# both sides with echo cancellation
export TARGET=192.168.1.1
gst-launch-1.0 \
  autoaudiosrc ! \
    webrtcdsp ! \
    queue ! \
    audioresample ! \
    opusenc audio-type=voice inband-fec=FALSE frame-size=20 bitrate=64000 dtx=TRUE ! \
    rtpopuspay ! \
    udpsink host=$TARGET port=5000 async=FALSE \
  udpsrc port=5000 caps="application/x-rtp" ! \
    rtpjitterbuffer do-lost=TRUE do-retransmission=FALSE latency=10 ! \
    rtpopusdepay ! \
    opusdec plc=TRUE use-inband-fec=FALSE ! \
    audioresample ! \
    webrtcechoprobe ! \
    autoaudiosink
```

```
sudo docker run \
  --rm \
  -it \
  -e PULSE_SERVER=unix:/run/user/1000/pulse/native \
  -v ${XDG_RUNTIME_DIR}/pulse/native:/run/user/1000/pulse/native \
  -v ~/.config/pulse/cookie:/root/.config/pulse/cookie \
  --group-add $(getent group audio | cut -d: -f3) \
  registry.gitlab.inria.fr/qdufour/donar 
```
