#include "url.h"

char* url_get_port(char* out, char* in) {
  sscanf(in, "%*[a-z]:%*[a-z]:%*[a-zA-Z0-9.]:%[0-9]", out);
  return out;
}

int url_get_port_int(char* in) {
  int out;
  sscanf(in, "%*[a-z]:%*[a-z]:%*[a-zA-Z0-9.]:%d", &out);
  return out;
}
