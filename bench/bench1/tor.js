'use strict'

const granax = require('granax')
const fs = require('fs')

const init = (stream_fd, circ_fd) => new Promise((resolve, reject) => {
  const tor = granax(null, {
    UseEntryGuards: 1,
    NumEntryGuards: 100000,
    NumPrimaryGuards: 100000,
    NumDirectoryGuards: 100000,
    SocksPort: "auto IsolateClientAddr IsolateSOCKSAuth IsolateClientProtocol IsolateDestPort IsolateDestAddr OnionTrafficOnly",
  })

  //tor.process.stdout.setEncoding("utf-8")
  //tor.process.stdout.on('data', console.info)
  
  tor.on('error', err => { console.error("Unable to start Tor", err); reject(err)})
  tor.on('close', () => console.error("Control socket has been closed"))
  tor.on('STREAM', function(data) {
    fs.write(stream_fd, `${data}\n`, () => {})
  })
  tor.on('CIRC', function(data) {
    fs.write(circ_fd, `${data}\n`, () => {})
  })

  tor.on('ready', () =>
    tor.addEventListeners(['STREAM', 'CIRC'], () => {
      console.info("Tor module has been inited")
      resolve(tor)
  }))
})

module.exports = { init: init }
