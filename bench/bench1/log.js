'use strict'

const fs = require('fs')

const get_write_fd = path => new Promise((resolve, reject) => {
  fs.open(path, 'wx', (err, fd) => err ? reject(err) : resolve(fd))
})

const init_files = new Promise((resolve, reject) => {
  const prefix = Date.now().toString()
  Promise.all([get_write_fd(`logs/${prefix}-stream.log`), get_write_fd(`logs/${prefix}-circuit.log`)])
    .then(values => {
      console.info("Files module has been inited")
      resolve(values)
    }).catch(err => {
      console.error("Files module failed with", err)
      reject(err)
    })
})

module.exports = {init: init_files, get_write_fd: get_write_fd}
