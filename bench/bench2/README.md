```
sudo docker build -t superboum/am64_tor_lat:v1 .
```

```
sudo docker run -d -t -i -p 443:443a --name tor_relay -v `pwd`/log:/log superboum/am64_tor_lat:v1 bash -c 'tor -f torrc > /log/tor.log 2>&1'
```

Wait ~2 hours

```
sudo docker run -d -t -i --net=container:tor_relay superboum/am64_tor_lat:v1 bash -c 'node client.js TryToDoResearch,IPredator > /log/xp-stdout.log 2> /log/xp-stderr.log'
```
