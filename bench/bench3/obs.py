from stem.control import Controller, EventType
from stem import CircStatus, CircEvent, HiddenServiceState
import time
import datetime
import functools

per_circuit = {}
per_hs = {}

hs_hr = {
  HiddenServiceState.HSCR_CONNECTING: 'CLIENT - Création du point de RDV en cours...',
  HiddenServiceState.HSCI_CONNECTING: 'CLIENT - Connexion au point d\'introduction...',
  HiddenServiceState.HSCR_ESTABLISHED_IDLE: 'CLIENT - Création du point de RDV terminée',
  HiddenServiceState.HSCI_INTRO_SENT: 'CLIENT - RDV envoyé via le point d\'introduction',
  HiddenServiceState.HSCR_ESTABLISHED_WAITING: 'CLIENT - Le point de RDV attend l\'Onion Service',
  HiddenServiceState.HSCI_DONE: 'CLIENT - Fermeture de la connexion avec le point d\'introduction',
  HiddenServiceState.HSCR_JOINED: 'CLIENT - L\'Onion Service est connecté, le lien est prêt'
}

def handle_circ(event):
  print('.', end='', flush=True)
  if event.id not in per_circuit: per_circuit[event.id] = []
  per_circuit[event.id].append((EventType.CIRC, datetime.datetime.now(), event))

def handle_circ_minor(event):
  print('+', end='', flush=True)
  if event.id not in per_circuit: per_circuit[event.id] = []
  if len(per_circuit[event.id]) > 0 and per_circuit[event.id][-1][2].hs_state == event.hs_state: return
  per_circuit[event.id].append((EventType.CIRC_MINOR, datetime.datetime.now(), event))

with Controller.from_port(port = 9051) as controller:
  controller.authenticate()
  controller.add_event_listener(handle_circ, EventType.CIRC)
  controller.add_event_listener(handle_circ_minor, EventType.CIRC_MINOR)
  input()
  print('-- results --')

  for circ, val in per_circuit.items():
    hs = functools.reduce(lambda acc, v: acc or v[2].rend_query, val, None)
    if hs not in per_hs: per_hs[hs] = []
    per_hs[hs] = per_hs[hs] + val

  for hs, val in per_hs.items():
    if hs == None: hs = "Not linked with HS"
    val.sort(key=lambda x: x[1])
    print(f"{hs}, delay: {val[-1][1] - val[0][1]}")
    for evt, ts, circ in val:
      if evt == EventType.CIRC_MINOR and circ.hs_state != None:
        print(f"\t{circ.id}, {ts}, {hs_hr[circ.hs_state]}")
      elif evt == EventType.CIRC and circ.status == CircStatus.LAUNCHED:
        print(f"\t{circ.id}, {ts}, création d'un nouveau circuit") 
